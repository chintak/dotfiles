(require 'cc-mode)
(require 'semantic)

;; (load-file "~/.emacs.d/cedet-1.0.1/common/cedet.el") ;; your cedet.el load path

;; Enable EDE only in C/C++
(require 'ede)
(global-ede-mode 1)

(global-semanticdb-minor-mode 1)
(global-semantic-idle-scheduler-mode 1)
(global-semantic-stickyfunc-mode 1)
(global-semantic-idle-completions-mode)
(global-semantic-decoration-mode)
(global-semantic-highlight-func-mode)
(global-semantic-show-unmatched-syntax-mode)

(semantic-mode 1)
;; (semantic-load-enable-excessive-code-helpers)

(defun alexott/cedet-hook ()
  (local-set-key "\C-c\C-j" 'semantic-ia-fast-jump)
  (local-set-key "\C-c\C-s" 'semantic-ia-show-summary))

(add-hook 'c-mode-common-hook 'alexott/cedet-hook)
(add-hook 'c-mode-hook 'alexott/cedet-hook)
(add-hook 'c++-mode-hook 'alexott/cedet-hook)
;; (add-hook 'cuda-mode-hook 'alexott/cedet-hook)

(provide 'setup-cedet)
