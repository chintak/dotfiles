#!/bin/bash

pip2 install --user powerline-status
pip2 install --user jedi

ln -sf `pwd`/tmux.conf ~/.tmux.conf
ln -sf `pwd`/tmux.conf ~/.tmux_conf

rm -rf ~/.vim
ln -sf `pwd`/vim ~/.vim
ln -sf `pwd`/vimrc ~/.vimrc

rm -rf ~/.emacs.d
rm -f ~/.emacs
ln -sf `pwd`/emacs.d ~/.emacs.d

grep -Fxq "TERM=xterm-256color" ~/.bash_profile
if [ -f ~/.bash_profile -a $? -ne 0 ]; then
    echo "TERM=xterm-256color\n$(cat ~/.bash_profile)" > ~/.bash_profile;
fi

grep -Fxq "TERM=xterm-256color" ~/.bashrc
if [ -f ~/.bashrc -a $? -ne 0 ]; then
    echo "TERM=xterm-256color\n$(cat ~/.bashrc)" > ~/.bashrc;
fi
