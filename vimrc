execute pathogen#infect()

autocmd! bufwritepost .vimrc source %

filetype off
filetype indent plugin on

set autowrite
set cursorline
set colorcolumn=80
set textwidth=80
set t_Co=256

syntax enable
if (has("termguicolors"))
  set termguicolors
endif
" set background=dark
" color spacemacs-theme

hi ColorColumn ctermbg=0
hi Search cterm=NONE ctermfg=darkgrey ctermbg=yellow
" hi CursorLine   cterm=NONE ctermbg=240
" hi Cursor cterm=NONE ctermbg=white ctermfg=100 guifg=darkred guibg=white
hi Cursor guifg=#121212 guibg=#afd700

set splitright
set wildmenu
set hlsearch
set ignorecase
set smartcase
set backspace=indent,eol,start
set nostartofline
set mouse=a
set cmdheight=1
set number
set pastetoggle=<F2>
set clipboard=unnamed

" tabs, spaces, autoindent
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

nmap <F9> :set noexpandtab<CR>:%retab!<CR>:set tabstop=2 shiftwidth=2 softtabstop=2 expandtab<CR>:retab<CR>
nmap <F10> :set noexpandtab<CR>:%retab!<CR>:set tabstop=4 shiftwidth=4 softtabstop=4 expandtab <CR>:retab<CR>

au Syntax python source ~/.vim/after/syntax/python.vim
au Syntax cpp source ~/.vim/after/syntax/cpp.vim
au Syntax cuda source ~/.vim/after/syntax/cpp.vim

set cindent
set autoindent

nnoremap <C-L> :nohl<CR><C-L>

set nobackup
set nowritebackup
set noswapfile

au FocusLost * :wa

let mapleader = ","

noremap <C-Z> :update<CR>

vnoremap < <gv  " better indentation
vnoremap > >gv  " better indentation

autocmd BufWritePre *.* :%s/\s\+$//e

noremap <C-n> :nohl<CR>

" RainbowParenthesis
let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

" NERDTreeTabs
let g:nerdtree_tabs_open_on_console_startup = 0
let g:nerdtree_tabs_autofind = 1
" autocmd vimenter * NERDTree
map <Leader>l <plug>NERDTreeTabsToggle<CR>
map <Leader>p <plug>NERDTreeTabsFind<CR>

" Navigating buffers
" nnoremap <Leader>l :ls<CR>
nnoremap <C-\> :edit<Space>
nnoremap <Leader>v :vsplit<Space>
" nnoremap <Leader>s :split<Space>
nnoremap < :bprevious<CR>
nnoremap > :bnext<CR>
nnoremap <Leader>k :close<CR>
nnoremap <Leader>w :bn<CR>:bd #<CR>
nnoremap <Leader>x :bw<CR>:tabclose<CR>
nnoremap <Leader>q :xa<CR>
nnoremap <Leader>s :w<CR>
map <c-h> <esc><c-w>j " down
map <c-j> <esc><c-w>k " up
map <c-l> <esc><c-w>l " right
map <c-k> <esc><c-w>h " left
let c = 1
while c <= 99
  execute "nnoremap " . c . "gb :" . c . "b\<CR>"
  let c += 1
endwhile

" Buffet
map <Leader>t :Bufferlist<CR>

" autocmd FileType python nnoremap <buffer> [[ ?^class\\|^\s*def<CR>
" autocmd FileType python nnoremap <buffer> ]] /^class\\|^\s*def<CR>

" search the word under the cursor
map f *
map F #
nmap f <Plug>MarkSearchOrCurNext
nmap F <Plug>MarkSearchOrCurPrev

" Navigating tabs
map n <esc>:tabprevious<CR>
map m <esc>:tabnext<CR>
nnoremap td  :tabclose<CR>
nnoremap <C-t> :tabnew<Space>

" vim-clang
let g:clang_c_options = '-std=gnu11'
let g:clang_cpp_options = '-std=c++11 -stdlib=libc++'

function! LoadCscope()
  let db = findfile("cscope.out", ".;")
  if (!empty(db))
    let path = strpart(db, 0, match(db, "/cscope.out$"))
    set nocscopeverbose " suppress 'duplicate connection' error
    exe "cs add " . db . " " . path
    set cscopeverbose
  endif
endfunction
au BufEnter /* call LoadCscope()

" set rtp+=$HOME/.local/lib/python2.7/site-packages/powerline/bindings/vim/
" Always show statusline
set laststatus=2
" set laststatus=1 statusline=%02n:%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P

" cd ~/.vim
" git clone https://github.com/kien/ctrlp.vim.git bundle/ctrlp.vim
let g:ctrlp_max_height = 30
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.o

" cd ~/.vim/bundle
" git clone git://github.com/davidhalter/jedi-vim.git
let g:jedi#usages_command = "<Leader>z"
" let g:jedi#use_splits_not_buffers = "right"

" Supertab
let g:SuperTabContextDefaultCompletionType = "<tab>"

" auto-paris
let g:AutoPairsFlyMode = 0

" vim-airline
let g:airline#extensions#tabline#enabled = 1
" let g:airline#extensions#tabline#left_sep = '#'
" let g:airline#extensions#tabline#left_alt_sep = '|'
" let g:airline#extensions#tabline#tab_min_count = 1
let g:airline#extensions#tabline#buffer_min_count = 2

" vim-cpp-enhanced-highlight
let g:cpp_class_scope_highlight = 1
let g:cpp_experimental_template_highlight = 1

" let g:flake8_show_in_gutter=1
" let g:flake8_show_in_file=0
" let g:flake8_show_quickfix=1
" autocmd BufWritePost *.py call Flake8()

" highlight Pmenu ctermfg=black ctermbg=gray
" highlight PmenuSel ctermfg=red  ctermbg=gray

set completeopt=longest,menuone
function! OmniPopup(action)
    if pumvisible()
        if a:action == 'j'
            return "\<C-N>"
        elseif a:action == 'k'
            return "\<C-P>"
        endif
    endif
    return a:action
endfunction

inoremap <silent><C-j> <C-R>=OmniPopup('j')<CR>
inoremap <silent><C-k> <C-R>=OmniPopup('k')<CR>

