export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

export CLICOLOR=1
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx
export PS1='\[\e[32m\]cs: \w\[\e[0m\]\n$ '

alias ipn='cd ~/ml_gp/Notebooks/; ipython notebook --profile=dark --no-browser'
alias ebook-convert='/Applications/calibre.app/Contents/MacOS/ebook-convert'

# Usage: compresspdf [input file] [output file] [screen*|ebook|printer|prepress]
compresspdf() {
    gs -sDEVICE=pdfwrite -dNOPAUSE -dQUIET -dBATCH -dPDFSETTINGS=/${3:-"screen"} -dCompatibilityLevel=1.4 -sOutputFile=$2 $1
}

function frameworkpython {
    if [[ ! -z "$VIRTUAL_ENV" ]]; then
        PYTHONHOME=$VIRTUAL_ENV /usr/local/bin/python "$@"
    else
        /usr/local/bin/python "$@"
    fi
}

if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
fi

alias new='tmux new -s'
alias att='tmux a -t'
alias emacx='/Applications/Emacs.app/Contents/MacOS/Emacs'

export NLTK_DATA=/Volumes/CHINTAK/data/nltk_data/

# Make the custom installed libs visible to CMake
export CMAKE_LIBRARY_PATH=/Users/chintaksheth/install/lib
# Add the torch lib
# . /Users/chintaksheth/git/torch/install/bin/torch-activate

export CAFFE_HOME=/Users/chintaksheth/work/caffe-fast-rcnn
# export CAFFE_HOME=/Users/chintaksheth/work/caffe-0.13.2
export PATH=$CAFFE_HOME/build/tools:/usr/local/mysql/bin:$PATH
export PYTHONPATH=$CAFFE_HOME/build/python:$PYTHONPATH
export DYLD_LIBRARY_PATH=$CAFFE_HOME/build/lib:/usr/local/gfortran/lib:$DYLD_LIBRARY_PATH
export WORKON_HOME=~/.envs
source /usr/local/bin/virtualenvwrapper.sh
export KERAS_BACKEND=tensorflow
alias gtx_mount='sshfs -p 1505 arya_g@gtx.ai:/home/arya_g/chintak/code ~/gtx/ -o defer_permissions -o volname=GTX'
alias gtxcon='ssh arya_g@gtx.ai -p 1505'
# alias g2_mount='sshfs -o IdentityFile=/Users/chintaksheth/.ssh/vision_team_arya.pem ubuntu@g22.ai:/home/ubuntu/digits-server/test/ ~/gtx/ -o defer_permissions -o volname=GTX'
alias gscon='ssh -Y -p 130 csheth@bigeye.cs.stonybrook.edu'
alias emacs='/usr/local/bin/emacs -nw'
function emacx() {
  echo 'calling emacs'
  /usr/local/bin/emacs -mm -fs ${@:1} &
}
export PATH=/Users/chintaksheth/Library/Python/2.7/bin/:/Users/chintaksheth/work/caffe-fast-rcnn/build/tools:/usr/local/mysql/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/Library/TeX/texbin:$PATH
eval "$(rbenv init -)"

# pyenv path
eval "$(pyenv init -)"
# eval "$(pyenv virtualenv-init -)"
export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"
if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi
export PATH=~/.pyenv/shims:$PATH

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

export PATH=$PATH:/Users/chintaksheth/bin
