setl noet
au Syntax python %retab!
setl ts=2
setl sw=2
setl et
au Syntax python RainbowParenthesesToggle
au Syntax python RainbowParenthesesLoadRound
au Syntax python RainbowParenthesesLoadBraces

