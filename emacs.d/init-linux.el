(global-hl-line-mode t)
(set-face-background 'hl-line "#3e4446")

;; install the missing packages
(setq package-list
  '(anzu xcscope yasnippet))  ;; function-args
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;; (add-to-list 'load-path "~/.emacs.d/custom")

;; setup yasnippet
(require 'yasnippet)
;; (yas-global-mode 1)
(yas-reload-all)
(add-hook 'prog-mode-hook #'yas-minor-mode)

;; better func tips and parsing
;; (require 'function-args)
;; (fa-config-default)
;; (set-default 'semantic-case-fold t)
;; (semantic-add-system-include "/usr/local/include" 'c++-mode)
(require 'xcscope)
(cscope-setup)

(eval-after-load "xcscope"
  '(progn
     (define-key cscope-command-map (kbd "M-c M-s") 'cscope-find-this-symbol)
     (define-key cscope-command-map (kbd "M-c M-d") 'cscope-find-global-definition)
     (define-key cscope-command-map (kbd "M-c M-c") 'cscope-find-called-function)
     (define-key cscope-command-map (kbd "M-c M-p") 'cscope-find-calling-this-funtcion)
     (define-key cscope-command-map (kbd "M-c M-e") 'cscope-select)))

;; custom settings
(global-set-key "\C-x\C-p" '(lambda() (interactive) (other-window -1)) )
(global-set-key "\C-x\C-o" 'other-window)
(global-set-key "\C-x\C-n" 'other-window)
(global-set-key "\M-g" 'goto-line)
(global-set-key "\C-c\C-w" 'compare-windows)

;; (add-to-list 'markdown-preview-javascript "http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML")

(global-set-key (kbd "C-S-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "C-S-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-S-<down>") 'shrink-window)
(global-set-key (kbd "C-S-<up>") 'enlarge-window)
(global-set-key (kbd "C-c i") 'idomenu)

;; register
(global-set-key (kbd "C-z") 'jump-to-register)
(global-set-key (kbd "C-S-z") 'window-configuration-to-register)

;;(defun save-all ()
;; (interactive)
;;  (save-some-buffers t))
(defun full-auto-save ()
  (interactive)
  (save-excursion
    (dolist (buf (buffer-list))
      (set-buffer buf)
      (if (and (buffer-file-name) (buffer-modified-p))
        (save-some-buffers t)))))
(add-hook 'focus-out-hook 'full-auto-save)

(provide 'init-linux)
