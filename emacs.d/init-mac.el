(setq mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil)))
(setq mouse-wheel-progressive-speed nil)

(global-hl-line-mode t)
(set-face-background 'hl-line "#3e4446")

;; install the missing packages
(setq package-list
  '(org markdown-mode latex-preview-pane auctex anzu xcscope yasnippet))  ;; function-args
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;; better func tips and parsing
; (require 'function-args)
; (fa-config-default)
; (set-default 'semantic-case-fold t)
; (semantic-add-system-include "/usr/local/include" 'c++-mode)
; (semantic-add-system-include "/usr/local/include/c++/5.3.0" 'c++-mode)
; (semantic-add-system-include "/System/Library/Frameworks/Python.framework/Versions/2.7/include/python2.7" 'c++-mode)

(add-to-list 'load-path "~/.emacs.d/custom")
(require 'setup-orgmode)
(if (equal emacs-major-version 25)
  (progn
    ;; (require 'setup-helm-gtags)
    (require 'setup-helm))
  (progn
    (require 'xcscope)
    (cscope-setup)))
;; (require 'setup-cedet)
;; (require 'setup-editing)
;; (require 'setup-desktop)

(setenv "PATH" (concat "/usr/local/bin:/Library/TeX/texbin:"
  (getenv "PATH")))
(setq exec-path
  (append '("/Library/TeX/texbin") '("/usr/local/bin") exec-path))

(set-face-attribute 'default nil :font "Source Code Pro Medium-15")
(set-frame-font "Source Code Pro Medium-15" nil t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(markdown-enable-math t)
 '(org-agenda-files
   (quote
	("/Users/chintaksheth/.org/todo.org" "/Users/chintaksheth/.org/notes.org")))
 '(org-agenda-todo-ignore-deadlines (quote all))
 '(org-agenda-todo-ignore-scheduled (quote future))
 '(org-agenda-todo-ignore-with-date t)
 '(org-agenda-todo-list-sublevels nil)
 '(org-capture-templates
   (quote
	(("l" "Leetcode q" entry
	  (file+headline "/Volumes/W/leet/problems.org" "Preparation")
	  "** %? \n  :PROPERTIES:\n  :COLUMNS:\n  :S: \n  :Link: [[%F][C++]]\n  :L: \n  :Company: \n  :Tags: \n  :END:\n  #+INCLUDE: \"%F\" src c++\n")))))

;; setup yasnippet
(require 'yasnippet)
;; (yas-global-mode 1)
(yas-reload-all)
(add-hook 'prog-mode-hook #'yas-minor-mode)

;; custom settings
(global-set-key "\C-x\C-p" '(lambda() (interactive) (other-window -1)) )
(global-set-key "\C-x\C-o" 'other-window)
(global-set-key "\C-x\C-n" 'other-window)
(global-set-key "\M-g" 'goto-line)
(global-set-key "\C-c\C-w" 'compare-windows)

;; (add-to-list 'markdown-preview-javascript "http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML")

(global-set-key (kbd "C-S-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "C-S-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-S-<down>") 'shrink-window)
(global-set-key (kbd "C-S-<up>") 'enlarge-window)
(global-set-key (kbd "C-c i") 'idomenu)

;; register
(global-set-key (kbd "C-z") 'jump-to-register)
(global-set-key (kbd "C-S-z") 'window-configuration-to-register)

;; auto save
(defun full-auto-save ()
  (interactive)
  (save-excursion
    (dolist (buf (buffer-list))
      (set-buffer buf)
      (if (and (buffer-file-name) (buffer-modified-p))
        (save-some-buffers t)))))
(add-hook 'focus-out-hook 'full-auto-save)

;; (org-agenda nil "a")

(provide 'init-mac)
