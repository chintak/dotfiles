so ~/.vim/after/syntax/stl.vim
setl noet
au Syntax cpp %retab!
setl ts=4
setl softtabstop=4
setl sw=4
setl noet
au Syntax cpp RainbowParenthesesActivate
au Syntax cpp RainbowParenthesesLoadRound
au Syntax cpp RainbowParenthesesLoadBraces
au Syntax cpp RainbowParenthesesToggleAll

