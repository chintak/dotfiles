(require 'package)
(add-to-list 'package-archives
 '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

;; install the missing packages
(setq package-list
  '(use-package smartparens auto-complete company workgroups2))
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

(require 'use-package)
(setq use-package-always-ensure t)

(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)
(menu-bar-mode -1)
(if window-system
  (tool-bar-mode -1))
(toggle-word-wrap)
(windmove-default-keybindings)
(setq windmove-wrap-around t)
(prefer-coding-system 'utf-8)
(setq buffer-file-coding-system 'utf-8)
(setq kill-whole-line t)
;; (global-linum-mode 1)
(setq make-backup-files nil)
(auto-save-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(set-face-background 'mode-line "#7D215D")
(set-face-background 'mode-line-inactive "#7D215D")
(defun nuke-all-buffers ()
  (interactive)
  (mapcar 'kill-buffer (buffer-list))
  (delete-other-windows))

(global-set-key (kbd "C-x K") 'nuke-all-buffers)

(show-paren-mode 1)  ;; show matching parens
(setq-default c-basic-offset 8
              tab-width 8
              indent-tabs-mode t)
(setq-default c-default-style "linux")

(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cc\\'" . c++-mode))

;; Package: projejctile
(use-package projectile
  :init
  (projectile-global-mode)
  (setq projectile-enable-caching t))
(setq projectile-require-project-root nil)

;; company
(use-package company
  :init
  (global-company-mode 1)
  (delete 'company-semantic company-backends))
;; (define-key c-mode-map  [(control tab)] 'company-complete)
;; (define-key c++-mode-map  [(control tab)] 'company-complete)

;; Package: smartparens
(require 'smartparens)
(show-smartparens-global-mode +1)
(smartparens-global-mode 1)

(require 'auto-complete)
(global-auto-complete-mode t)

;; Setup theme
;; (if (display-graphic-p) () ())
(use-package darkokai-theme
  :ensure t
  :config (load-theme 'darkokai t))

;; quick code navigation
(require 'imenu)
(global-set-key (kbd "M-0") 'imenu)

;; Highlight TODO word
(defun hilite-todos ()
  (highlight-regexp "TODO:?" 'hi-pink)
  (highlight-regexp "FIX:?" 'hi-green)
  (highlight-regexp "DEBUG:?" 'hi-green))
(add-hook 'c-mode-hook 'hilite-todos)
(add-hook 'python-mode-hook 'hilite-todos)

(if (eq system-type 'gnu/linux)
  (load "~/.emacs.d/init-linux.el")
  (load "~/.emacs.d/init-mac.el"))

;; configure workgroups2 for window and buffer management
(require 'workgroups2)
;; Change prefix key (before activating WG)
(setq wg-prefix-key (kbd "C-c w"))
;; Change workgroups session file
(setq wg-session-file "~/.emacs_workgroups")
;; Set your own keyboard shortcuts to reload/save/switch WGs:
(global-set-key (kbd "C-c w c") 'wg-create-workgroup)
(global-set-key (kbd "C-c w f") 'wg-find-session-file)
(global-set-key (kbd "C-c w r") 'wg-reload-session)
(global-set-key (kbd "C-c w s") 'wg-save-session)
(global-set-key (kbd "C-c w n") 'wg-switch-to-workgroup)
(global-set-key (kbd "C-c w p") 'wg-switch-to-previous-workgroup)
;; What to do on Emacs exit / workgroups-mode exit?
(setq wg-emacs-exit-save-behavior            nil)  ; Options: 'save 'ask nil
(setq wg-workgroups-mode-exit-save-behavior  nil)  ; Options: 'save 'ask nil
;; Mode Line changes
;; Display workgroups in Mode Line?
(setq wg-mode-line-display-on t)  ; Default: (not (featurep 'powerline))
(setq wg-flag-modified t)  ; Display modified flags as well
(setq wg-mode-line-decor-left-brace "["
      wg-mode-line-decor-right-brace "]"  ; how to surround it
      wg-mode-line-decor-divider ":")
(workgroups-mode 1)  ; put this one at the bottom of .emacs
;; (setq wg-morph-on t)

(defun recenter-draw ()
  (recenter '("don't redraw")))
(defun recenter-hook ()
  (interactive)
  (add-hook 'post-command-hook 'recenter-draw))
(call-interactively 'recenter-hook)

;; -------------- END -------------------
